package com.example.user.newsapplication.screens.base;

import android.support.v7.app.AppCompatActivity;

import com.example.user.newsapplication.NewsApplication;


public class EventActivity extends AppCompatActivity {

    @Override
    protected void onStart() {
        super.onStart();
        NewsApplication.get().getEventBus().register(this);
    }


}
