package com.example.user.newsapplication.utils;

import android.util.Log;


public class AppLog{

    public static void d(String debugLogMessage){
        Log.d("NewsApplication", debugLogMessage);
    }

    public static void e(String errorLogMessage, Exception e){
        Log.e("NewsApplication", errorLogMessage + ": " + e.getMessage());
    }
}
