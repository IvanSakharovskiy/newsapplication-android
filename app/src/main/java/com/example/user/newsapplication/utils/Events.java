package com.example.user.newsapplication.utils;


import com.example.user.newsapplication.NewsApplication;

public class Events {

    public static void post(Object object){
        NewsApplication.get().getEventBus().post(object);
    }

    
}
