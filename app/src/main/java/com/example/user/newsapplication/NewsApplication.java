package com.example.user.newsapplication;

import android.app.Application;

import org.greenrobot.eventbus.EventBus;

public class NewsApplication extends Application {

    private EventBus mEventBus;

    private static NewsApplication mInstance;

    public NewsApplication() {
        mInstance = new NewsApplication();
    }

    public static NewsApplication get(){
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mEventBus = new EventBus();
    }

    public EventBus getEventBus() {
        return mEventBus;
    }
}
